/*
 * sense_motor_current.c
 *
 *  Created on: Sep 2, 2020
 *      Author: user
 */

#include "sense_motor_current.h"

void initialiseSenseMotorCurrent(ADC_HandleTypeDef* h) {
	hadc = h;
	HAL_ADC_Start_DMA(hadc, motor_current_sense_adc_buffer, MOTOR_CURRENT_SENSE_ADC_BUFFER_LENGTH);
}

senseMotorCurrentADCCallback() {
	uint32_t counter = 0;
	uint16_t i;
	for(i = 0; i < MOTOR_CURRENT_SENSE_ADC_BUFFER_LENGTH; i++) {
		counter += motor_current_sense_adc_buffer[i];
	}

	uint32_t adc_value = (uint32_t) (counter / MOTOR_CURRENT_SENSE_ADC_BUFFER_LENGTH);
	float adc_voltage = 5.0 * adc_value / 4095.0;
	motor_current = adc_voltage / MOTOR_CURRENT_SENSE_RESISTANCE;
}
