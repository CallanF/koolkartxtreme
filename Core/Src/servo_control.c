/*
 * servo_control.c
 */
#include "servo_control.h"

static TIM_HandleTypeDef* htim;
static float currently_set_angle_d; //  not necessarily the current servo motor angle if we are in the middle of turning the motor

/*
 * internal utility functions, not for outside use
 */
void setDutyCycle(float duty_cycle) {
	__HAL_TIM_SET_COMPARE(htim, TIM_CHANNEL_4, (uint16_t) STEERING_SERVO_MAX_COUNTER*duty_cycle);
}

/*
 * PUBLICLY AVAILABLE FUNCTIONS
 */
void initialiseServoControl(TIM_HandleTypeDef* h) {
	htim = h;
	HAL_TIM_PWM_Start(htim, TIM_CHANNEL_4);
}

void setServoAngle(float angle_d) {
	// convert the angle to a duty cycle
	float duty_cycle;
	if(angle_d <= STEERING_SERVO_MIN_ANGLE) {
		duty_cycle = STEERING_SERVO_MIN_DC;
		currently_set_angle_d = STEERING_SERVO_MIN_ANGLE;
	} else if (angle_d >= STEERING_SERVO_MAX_ANGLE) {
		duty_cycle = STEERING_SERVO_MAX_DC;
		currently_set_angle_d = STEERING_SERVO_MAX_ANGLE;
	} else {
		duty_cycle = STEERING_SERVO_MIN_DC + (STEERING_SERVO_MAX_DC - STEERING_SERVO_MIN_DC) * (angle_d - STEERING_SERVO_MIN_ANGLE)/(STEERING_SERVO_MAX_ANGLE - STEERING_SERVO_MIN_ANGLE);
		currently_set_angle_d = angle_d;
	}

	// set the duty cycle for the PWM signal
	setDutyCycle(duty_cycle);
}

void setServoAngleRelative(float relative_angle_d) {
	setServoAngle(currently_set_angle_d + relative_angle_d);
}

float getCurrentlySetServoAngle(void) {
	return currently_set_angle_d;
}
