/*
 * servo_control.h
 *
 *  Created on: Aug 20, 2020
 *      Author: user
 */

#ifndef SRC_SERVO_CONTROL_H_
#define SRC_SERVO_CONTROL_H_

#define STEERING_SERVO_MAX_COUNTER 16512
#define STEERING_SERVO_MIN_ANGLE -90
#define STEERING_SERVO_MAX_ANGLE 90
#define STEERING_SERVO_MIN_DC 0.05
#define STEERING_SERVO_MAX_DC 0.1
#define STEERING_SERVO_CENTER_DC 0.075

#include "main.h"

void initialiseServoControl(TIM_HandleTypeDef* h);
void setServoAngle(float angle_d); // the angle to set the steering servo to
void setServoAngleRelative(float relative_angle_d); // a relative number of degrees to turn the steering servo
float getCurrentlySetServoAngle(void);

#endif /* SRC_SERVO_CONTROL_H_ */
