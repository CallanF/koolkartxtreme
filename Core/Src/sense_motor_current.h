/*
 * sense_motor_current.h
 *
 *  Created on: Sep 2, 2020
 *      Author: user
 */

#ifndef SRC_SENSE_MOTOR_CURRENT_H_
#define SRC_SENSE_MOTOR_CURRENT_H_

#define MOTOR_CURRENT_SENSE_RESISTANCE 0.02167
#define MOTOR_CURRENT_SENSE_ADC_BUFFER_LENGTH 500 // 50ms at buffer length 1000 and ADC clock division 8 (including time required to average)

#include "main.h"

uint32_t motor_current_sense_adc_buffer[MOTOR_CURRENT_SENSE_ADC_BUFFER_LENGTH];
ADC_HandleTypeDef* hadc;
float motor_current;

void initialiseSenseMotorCurrent(ADC_HandleTypeDef* hadc);
void senseMotorCurrentADCCallback();

#endif /* SRC_SENSE_MOTOR_CURRENT_H_ */
